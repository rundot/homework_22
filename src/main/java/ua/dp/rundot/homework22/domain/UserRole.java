package ua.dp.rundot.homework22.domain;

/**
 * Created by emaksimovich on 17.12.16.
 */
public enum UserRole {
    ADMIN,
    CUSTOMER;
}
