package ua.dp.rundot.homework22.domain;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;

import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * Created by emaksimovich on 30.11.16.
 */
@JsonAutoDetect
public class Account {

    private long accountNumber;
    private BigDecimal balance;
    private Timestamp creationDate;
    private String currency;
    private boolean blocked;
    private long customerId;
    private long userId;

    public Account() {
        this.creationDate = new Timestamp(System.currentTimeMillis());
    }

    public Account(long accountNumber, BigDecimal balance, Timestamp creationDate, String currency, boolean blocked, long customerId, long userId) {
        this.accountNumber = accountNumber;
        this.balance = balance;
        this.creationDate = creationDate;
        this.currency = currency;
        this.blocked = blocked;
        this.customerId = customerId;
        this.userId = userId;
    }

    public long getAccountNumber() {
        return accountNumber;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) { this.balance = balance; }

    public Timestamp getCreationDate() {
        return creationDate;
    }

    public long getUserId() {
        return userId;
    }

    public String getCurrency() {
        return currency;
    }

    public boolean isBlocked() {
        return blocked;
    }

    public long getCustomerId() {
        return customerId;
    }

    @Override
    public String toString() {
        return "Account{" +
                "accountNumber=" + accountNumber +
                ", balance=" + balance +
                ", creationDate=" + creationDate +
                ", currency='" + currency + '\'' +
                ", blocked=" + blocked +
                ", customerId=" + customerId +
                '}';
    }
}
