package ua.dp.rundot.homework22.service;

import ua.dp.rundot.homework22.dao.PostgreSQL.TransactionDaoImpl;
import ua.dp.rundot.homework22.dao.TransactionDao;
import ua.dp.rundot.homework22.domain.Transaction;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by emaksimovich on 30.11.16.
 */
public class TransactionService {

    private static TransactionDao transactionDao = new TransactionDaoImpl();

    public static List<Transaction> list() {
        return transactionDao.list();
    }

    public static List<Transaction> listByAccount(long accountNumber) {
        return transactionDao.list().stream()
                .filter(transaction -> (transaction.getAccountNumber() == accountNumber))
                .collect(Collectors.toList());
    }

    public static int deleteByAccount(Long accountNumber) {
        return transactionDao.deleteByAccount(accountNumber);
    }

    public static Long save(Transaction transaction) {
        switch (transaction.getOperationType()) {
            case "PUT": AccountService.increaseBalance(transaction.getAccountNumber(), transaction.getAmount()); break;
            case "WTH": AccountService.decreaseBalance(transaction.getAccountNumber(), transaction.getAmount()); break;
        }
        return transactionDao.save(transaction);
    }

    public static Transaction get(long id) {
        return transactionDao.get(id);
    }

    public static int delete(Long id) {
        return transactionDao.delete(id);
    }


}


