package ua.dp.rundot.homework22.service;

import ua.dp.rundot.homework22.dao.CustomerDao;
import ua.dp.rundot.homework22.dao.PostgreSQL.CustomerDaoImpl;
import ua.dp.rundot.homework22.domain.Account;
import ua.dp.rundot.homework22.domain.Customer;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by emaksimovich on 30.11.16.
 */
public class CustomerService {

    private static CustomerDao customerDao = new CustomerDaoImpl();

    public static long save(Customer customer) {
        long id = customerDao.save(customer);
        AccountService.save(new Account(0l,
                BigDecimal.valueOf(0),
                new Timestamp(System.currentTimeMillis()),
                "UAH",
                false,
                id,
                0l));
        return id;
    }

    public static void clearUserId(Long id) {
        customerDao.list()
                .stream()
                .filter(customer -> (customer.getUserId() == id))
                .collect(Collectors.toList())
                .stream()
                .forEach(customer -> {
                    customer.setUserId(null);
                    update(customer);
                });
    }

    public static void update(Customer customer) {
        customerDao.update(customer);
    }

    public static int delete(Long customerNumber) {
        List<Account> accountList = AccountService.listByCustomer(customerNumber);
        accountList.stream().forEach(account -> AccountService.delete(account.getAccountNumber()));
        return customerDao.delete(customerNumber);
    }

    public static Customer get(Long id) {
        return customerDao.get(id);
    }

    public static List<Customer> list() {
        return customerDao.list();
    }


}
