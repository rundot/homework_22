package ua.dp.rundot.homework22.service;

import ua.dp.rundot.homework22.dao.AccountDao;
import ua.dp.rundot.homework22.dao.PostgreSQL.AccountDaoImpl;
import ua.dp.rundot.homework22.dao.PostgreSQL.TransactionDaoImpl;
import ua.dp.rundot.homework22.domain.Account;
import ua.dp.rundot.homework22.domain.Transaction;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by emaksimovich on 30.11.16.
 */
public class AccountService {

    private static AccountDao accountDao = new AccountDaoImpl();

    public static Account get(Long id) {
        return accountDao.get(id);
    }

    public static Long save(Account account) {
        long id = accountDao.save(account);
        if (account.getBalance() != BigDecimal.ZERO) {
            Transaction transaction = new Transaction(
                    0l,
                    account.getBalance(),
                    new Timestamp(System.currentTimeMillis()),
                    "PUT",
                    id,
                    0l);
            new TransactionDaoImpl().save(transaction);
        }
        return id;
    }

    public static List<Account> listByCustomer(Long id) {
        return accountDao.list().stream()
                .filter(account -> (account.getCustomerId() == id))
                .collect(Collectors.toList());
    }

    public static void increaseBalance(Long id, BigDecimal amount) {
        Account account = accountDao.get(id);
        account.setBalance(amount.add(account.getBalance()));
        accountDao.update(account);
    }

    public static void decreaseBalance(Long id, BigDecimal amount) {
        Account account = accountDao.get(id);
        account.setBalance(amount.subtract(account.getBalance()));
        accountDao.update(account);
    }

    public static List<Account> list() {
        return accountDao.list();
    }

    public static int delete(Long accountNumber) {
        TransactionService.deleteByAccount(accountNumber);
        return accountDao.delete(accountNumber);
    }

}
