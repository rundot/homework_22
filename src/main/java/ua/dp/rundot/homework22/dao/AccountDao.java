package ua.dp.rundot.homework22.dao;

import ua.dp.rundot.homework22.domain.Account;

import java.util.List;
/**
 * Created by emaksimovich on 30.11.16.
 */
public interface AccountDao {

    Account get(Long accountNumber);

    Long save(Account account);

    void update(Account account);

    int delete(Long accountNumber);

    List<Account> list();

    int deleteByCustomer(Long customerId);
}
