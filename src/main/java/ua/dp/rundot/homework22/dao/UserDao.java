package ua.dp.rundot.homework22.dao;
import ua.dp.rundot.homework22.domain.User;

import java.util.List;

/**
 * Created by emaksimovich on 17.12.16.
 */
public interface UserDao {

    Long save(User user);

    User get(Long id);

    int update(User user);

    int delete(Long id);

    List<User> list();

}
