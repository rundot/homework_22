package ua.dp.rundot.homework22.dao;

import ua.dp.rundot.homework22.domain.Transaction;

import java.util.List;

/**
 * Created by emaksimovich on 30.11.16.
 */
public interface TransactionDao {

    Transaction get(Long Id);

    Long save(Transaction Transaction);

    void update(Transaction Transaction);

    int delete(Long id);

    List<Transaction> list();

    int deleteByAccount(Long accountId);

}
