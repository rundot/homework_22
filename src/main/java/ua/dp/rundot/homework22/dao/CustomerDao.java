package ua.dp.rundot.homework22.dao;


import ua.dp.rundot.homework22.domain.Customer;

import java.util.List;

/**
 * Created by emaksimovich on 30.11.16.
 */
public interface CustomerDao {

    Customer get(Long Id);

    Long save(Customer customer);

    void update(Customer customer);

    int delete(Long id);

    List<Customer> list();

}
