package ua.dp.rundot.homework22.controller;

import ua.dp.rundot.homework22.domain.User;
import ua.dp.rundot.homework22.service.UserService;
import ua.dp.rundot.homework22.util.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by emaksimovich on 18.12.16.
 */
public class LoginServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("login.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String action = req.getParameter("action");
        switch (action) {
            case "login":
                String login = req.getParameter("login");
                String password = req.getParameter("password");
                Logger.log(login + ":" + password);
                if (UserService.isValid(login, password)) {
                    HttpSession session = req.getSession();
                    User user = UserService.get(login, password);
                    session.setAttribute("login", user.getLogin());
                    session.setAttribute("password", password);
                    session.setAttribute("name", user.getName());
                    session.setAttribute("role", user.getRole());
                    session.setAttribute("userId", user.getId());
                    resp.sendRedirect("/bank/index");
                } else {
                    req.setAttribute("result", "ERROR: incorrect login/password");
                    req.getRequestDispatcher("/login.jsp").forward(req, resp);
                }
                break;
            case "register":
                resp.sendRedirect("/bank/register");
                break;
        }
    }
}
