package ua.dp.rundot.homework22.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import ua.dp.rundot.homework22.domain.Account;
import ua.dp.rundot.homework22.domain.Customer;
import ua.dp.rundot.homework22.domain.Transaction;
import ua.dp.rundot.homework22.service.AccountService;
import ua.dp.rundot.homework22.service.CustomerService;
import ua.dp.rundot.homework22.service.TransactionService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by emaksimovich on 10.12.16.
 */
public class IndexServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/home.jsp").forward(req, resp);
    }

}
