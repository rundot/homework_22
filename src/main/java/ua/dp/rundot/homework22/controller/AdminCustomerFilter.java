package ua.dp.rundot.homework22.controller;

import ua.dp.rundot.homework22.domain.UserRole;
import ua.dp.rundot.homework22.service.UserService;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by emaksimovich on 19.12.16.
 */
public class AdminCustomerFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) servletRequest;
        HttpSession session = req.getSession();
        UserRole role  = (UserRole)session.getAttribute("role");
        if (role == UserRole.ADMIN) {
            filterChain.doFilter(req, servletResponse);
        }
        else {
            HttpServletResponse resp = (HttpServletResponse) servletResponse;
            resp.sendRedirect("/bank/index");
        }
    }

    @Override
    public void destroy() {

    }

}
