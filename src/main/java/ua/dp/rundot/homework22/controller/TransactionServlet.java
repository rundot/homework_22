package ua.dp.rundot.homework22.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import ua.dp.rundot.homework22.domain.Transaction;
import ua.dp.rundot.homework22.service.AccountService;
import ua.dp.rundot.homework22.service.TransactionService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * Created by emaksimovich on 18.12.16.
 */
public class TransactionServlet extends HttpServlet {

    Pattern transactionListPattern = Pattern.compile("/bank/transactions");
    Pattern transactionNewPattern = Pattern.compile("/bank/transactions/new");
    Pattern transactionAddPattern = Pattern.compile("/bank/transactions/add");
    Pattern transactionDeletePattern = Pattern.compile("/bank/transactions/delete");

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String requestURI = req.getRequestURI();

        if (transactionListPattern.matcher(requestURI).matches()) {
            req.setAttribute("transactionList", TransactionService.list());
            req.getRequestDispatcher("/transactions.jsp").forward(req, resp);
            return;
        }

        if (transactionNewPattern.matcher(requestURI).matches()) {
            req.setAttribute("accountList", AccountService.list());
            req.setAttribute("transactionList", TransactionService.list());
            req.getRequestDispatcher("/transaction_form.jsp").forward(req, resp);
            return;
        }

        resp.sendError(404);

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String requestURI = req.getRequestURI();

        BufferedReader reader = new BufferedReader(new InputStreamReader(req.getInputStream()));
        String json = "";
        if (reader != null) {
            json = reader.readLine();
        }
        ObjectMapper mapper = new ObjectMapper();

        if (transactionAddPattern.matcher(requestURI).matches()) {
            Transaction transaction = mapper.readValue(json, Transaction.class);
            Long id = TransactionService.save(transaction);
            Transaction respTransaction = TransactionService.get(id);
            Map<String, String> result = new HashMap<>();
            result.put("id", String.valueOf(respTransaction.getId()));
            result.put("accountNumber", String.valueOf(respTransaction.getAccountNumber()));
            result.put("date", respTransaction.getDate().toString());
            result.put("amount", respTransaction.getAmount().toString());
            result.put("operationType", respTransaction.getOperationType());
            resp.setContentType("application/json");
            mapper.writeValue(resp.getOutputStream(), result);
            return;
        }

        if (transactionDeletePattern.matcher(requestURI).matches()) {
            Long id = mapper.readValue(json, long.class);
            Long res = TransactionService.delete(id) > 0? id : 0;
            resp.setContentType("application/json");
            mapper.writeValue(resp.getOutputStream(), res);
            return;
        }
    }
}
