package ua.dp.rundot.homework22.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by emaksimovich on 17.12.16.
 */
public class SingleConnection {

    private static Connection connection;

    private SingleConnection() {};

    public static Connection getInstance() {
        if (connection == null) {
            try {
                Class.forName("org.postgresql.Driver");
                connection = DriverManager.getConnection("jdbc:postgresql://rundot.dp.ua:5432/bank", "levelup", "levelup");
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return connection;
    }

}
