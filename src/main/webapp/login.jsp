<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="/bank/css/style.css" />
    <title>Login</title>
</head>
<body>
    <div id="message">${result}</div>
    <div id="login">
        <form action="login" method="POST">
            <input class="login" type="text" name="login" placeholder="login" /><br />
            <input class="login" type="password" name="password" placeholder="password" /><br />
            <button class="login_submit" type="submit" name="action" value="login" />Sign In</button>
            <button class="signup_submit" type="submit" name="action" value="register" />Sign Up</button>
        </form>
    </div>
    <div id="info">
        user with admin privileges <b>admin:admin</b><br />
        user with customer privileges <b>user:user</b><br />
        bitBucket repository: <a href="https://bitbucket.org/rundot/homework_22/src">https://bitbucket.org/rundot/homework_22/src</a>
    </div>
</body>
</html>