<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="/bank/css/style.css" />
    <title>Transactions</title>
</head>
<body>
<div id="navigation_menu">
    <ul>
        <li><a href="/bank/customers">Customers</a></li>
        <li><a href="/bank/accounts">Accounts</a></li>
        <li><a id="current_page" href="/bank/transactions">Transactions</a></li>
        <c:if test="${sessionScope.role == 'ADMIN'}">
            <li><a href="/bank/users">Users</a></li>
        </c:if>
        <li id="home"><a href="/bank/index"><img src="/bank/img/home.png" /></a></li>
    </ul>
</div>
<div id="main_content">
    <h1>Transactions</h1>
    <a href="/bank/transactions/new"><div id="create_transaction">Create transaction</div></a>
    <table id="transaction_list">
        <tr>
            <th>Amount</th>
            <th>Date</th>
            <th>Operation type</th>
            <th>Account number</th>
            <th></th>
        </tr>
        <c:forEach var="transaction" items="${transactionList}">
            <c:if test="${(sessionScope.role == 'ADMIN') || (sessionScope.userId == transaction.userId)}">
                <tr id=${transaction.id}>
                    <td>${transaction.amount}</td>
                    <td>${transaction.date}</td>
                    <td>${transaction.operationType}</td>
                    <td>${transaction.accountNumber}</td>
                    <td><div id="transaction_delete"><img src="/bank/img/delete.png" /></div></td>
                </tr>
            </c:if>
        </c:forEach>
        </table>
    </div>
    <script type="text/javascript" src="/bank/javascript/jquery.js"></script>
    <script type="text/javascript" src="/bank/javascript/script.js"></script>
</body>
</html>