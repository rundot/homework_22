<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="/bank/css/style.css" />
    <title>Users</title>
</head>
<body>
<div id="navigation_menu">
    <ul>
        <li><a href="/bank/customers">Customers</a></li>
        <li><a href="/bank/accounts">Accounts</a></li>
        <li><a href="/bank/transactions">Transactions</a></li>
        <c:if test="${sessionScope.role == 'ADMIN'}">
            <li><a id="current_page" href="/bank/users">Users</a></li>
        </c:if>
        <li id="home"><a href="/bank/index"><img src="/bank/img/home.png" /></a></li>
    </ul>
</div>
<div id="main_content">
    <h1>Users</h1>
    <a href="/bank/users/new"><div id="create_user">Create user</div></a>
    <table id="user_list">
        <tr>
            <th>Login</th>
            <th>Name</th>
            <th>Role</th>
            <th>Customer</th>
            <th></th>
        </tr>
        <c:forEach var="user" items="${userList}">
            <tr id="${user.id}">
                <td>${user.login}</td>
                <td>${user.name}</td>
                <td>${user.role}</td>
                <td>${user.customer}</td>
                <td><div id="user_delete"><img src="/bank/img/delete.png" /></div></td>
            </tr>
        </c:forEach>
    </table>
</div>
    <script type="text/javascript" src="/bank/javascript/jquery.js"></script>
    <script language="javascript" type="text/javascript" src="/bank/javascript/script.js"></script>
</body>
</html>